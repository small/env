<?php

namespace Small\tests;

use PHPUnit\Framework\TestCase;
use Small\Env\Env;

class EnvTest extends TestCase
{

    public function testGetSystem()
    {

        $env = new Env();
        self::assertEquals('test', $env->get('TEST_VAR'));

    }

    public function testParse()
    {

        $env = (new Env())
            ->parseFile(__DIR__ . '/data/.env');

        self::assertEquals('xyz123', $env->get('SIMPLE'));
        self::assertEquals('Multiple\Lines and variable substitution: xyz123', $env->get('INTERPOLATED_VAR'));
        self::assertEquals('raw text without variable interpolation', $env->get('NON_INTERPOLATED'));
        self::assertEquals("long text here, xyz123\ne.g. a private SSH key\n", $env->get('MULTILINE_INTERPOLATED'));
        self::assertEquals("long text here, xyz123\ne.g. a private SSH key\n", $env->get('MULTILINE'));

    }

}