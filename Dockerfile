FROM php:8.2-cli

# install composer
RUN apt-get update && \
    apt-get install -y git zip
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/bin --filename=composer
RUN chmod 755 /usr/bin/composer

# system setup
RUN mkdir /usr/lib/small-env
WORKDIR /usr/lib/small-env

# test envvars
ENV TEST_VAR=test

ENTRYPOINT sleep infinity