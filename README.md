# small-env

## About

This project intend to manage env vars from system or dot env files

## Install

Require the package with composer:
```
composer require small/env
```

## Usage

### Get a env var

```php
$env = new \Small\Env\Env();
$env->get('test');
```

Note that if var not loaded in class, it will ask system for value and store result in class

### Parse a dot env file
```php
$env = new \Small\Env\Env();
$env->parseFile(__DIR__ . '/.env');
echo $env->get('MY_VAR');
```

### Force value from system env var

```php
$env = new \Small\Env\Env();
$env->parseFile(__DIR__ . '/.env');
$env->grabFromSystem('MY_VAR');
echo $env->get('MY_VAR');
```

### test

To run unit tests, use :
```bash
$ bin/test --build
```