<?php

/**
 * This file is a part of small-env
 * Copyright 2023- - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Small\Env\Exception;

class MissingException extends EnvException
{

}