<?php

/**
 * This file is a part of small-env
 * Copyright 2023- - Sébastien Kus
 * Under GNU GPL V3 licence
 */

namespace Small\Env;

use Small\Env\Exception\FileNotFoundException;
use Small\Env\Exception\LineNotManagedException;
use Small\Env\Exception\MalformedException;
use Small\Env\Exception\MissingException;

class Env
{

    // List of known env vars
    protected array $vars = [];

    /**
     * Get env var
     * @param string $var
     * @return string
     */
    public function get(string $var): string
    {
        // If not exists in class
        if (!array_key_exists($var, $this->vars)) {
            // try to get if from system
            return $this->grabFromSystem($var);
        }

        return $this->vars[$var];
    }

    /**
     * Get env var from system and overwrite actual value
     * @param string $var
     * @return string
     * @throws MissingException
     */
    public function grabFromSystem(string $var): string
    {
        $this->checkNameFormat($var);

        $value = getenv($var);
        if ($value === false) {
            throw new MissingException('The env var ' . $var . ' is not defined');
        }

        $this->vars[$value] = $value;

        return $value;
    }

    /**
     * Parse file as dot env file
     * @param string $file
     * @return $this
     * @throws FileNotFoundException
     * @throws LineNotManagedException
     * @throws MalformedException
     */
    public function parseFile(string $file): Env
    {

        if (!is_file($file)) {
            throw new FileNotFoundException('File ' . $file . ' does not exists');
        }

        $f = fopen($file, 'r');

        while ($line = fgets($f)) {

            list($var, $startPos) = $this->getVarFromFileLine($line);
            $this->checkNameFormat($var);

            $value = '';
            try {
                $value = $this->parseSingle($line, $startPos);
            } catch (LineNotManagedException) {
                $multiline = false;
                switch (substr($line, $startPos, 3)) {
                    case '"""':
                        $multiline = true;
                        do {
                            $nextLine = fgets($f);
                            $line .= $nextLine;
                        } while(!str_contains($nextLine, '"""') || $nextLine === false);
                        if ($line === false) {
                            throw new MalformedException('Unexpected end of file');
                        }
                        if (!str_starts_with($nextLine, '"""')) {
                            throw new MalformedException('Ending string for multilines escaped value must be at the beginning of line');
                        }
                        if (trim(substr($nextLine, 4)) != '' && trim(substr($nextLine, 3, 1) != '#')) {
                            throw new MalformedException('Ending string for multilines escaped value can contains only additionnal comments');
                        }
                        break;
                    case "'''":
                        $multiline = true;
                        do {
                            $nextLine = fgets($f);
                            $line .= $nextLine;
                        } while(!str_contains($nextLine, "'''") || $nextLine === false);
                        if ($line === false) {
                            throw new MalformedException('Unexpected end of file');
                        }
                        if (!str_starts_with($nextLine, "'''")) {
                            throw new MalformedException('Ending string for multilines escaped value must be at the beginning of line');
                        }
                        if (trim(substr($nextLine, 3)) != '' && trim(substr($nextLine, 3, 1) != '#')) {
                            throw new MalformedException('Ending string for multilines escaped value can contains only additionnal comments');
                        }
                        break;
                }

                switch ($line[$startPos]) {
                    case '\'':
                        $value = $this->parseEscaped($line, $startPos, false, $multiline);
                        break;
                    case '"':
                        $value = $this->parseEscaped($line, $startPos, true, $multiline);
                        break;
                }
            }

            $this->vars[$var] = $value;
        }

        return $this;
    }

    /**
     * Env var check name format
     * @param $var
     * @return $this
     * @throws MalformedException
     */
    protected function checkNameFormat($var): Env
    {
        $match = preg_match('/^[a-zA-Z_]+[a-zA-Z0-9_]*$/', $var);
        if ($match === 0 || $match === false) {
            throw new MalformedException('The env var ' . $var . ' is malformed');
        }

        return $this;
    }

    /**
     * Extract var from line
     * @param string $line
     * @return array
     * @throws MalformedException
     */
    private function getVarFromFileLine(string $line): array
    {
        $var = '';
        for ($i = 0; $i < strlen($line); $i++) {
            if ($line[$i] != '=') {
                $var .= $line[$i];
            } else {
                return [$var, $i + 1];
            }
        }

        throw new MalformedException('The file contains a line without affectation');
    }

    /**
     * Parse single value
     * @param string $line
     * @param int $startPos
     * @return string
     */
    private function parseSingle(string $line, int $startPos): string
    {
        if (in_array($line[$startPos], [' ', "\t", "\\"])) {
            throw new MalformedException('Value can\'t start with blank char');
        }

        if ($line[$startPos] == '\'' || $line[$startPos] == '"') {
            throw new LineNotManagedException('Not a single value line');
        }

        $value = '';
        for ($i = $startPos; $i < strlen($line); $i++) {
            if ($line[$i] == '#' || $line[$i] == "\n" || $line == "\r") {
                return $value;
            }

            $value .= $line[$i];
        }

        return $value;
    }

    /**
     * Parse escaped value
     * @param string $line
     * @param int $startPos
     * @param $interpolated
     * @return string
     */
    private function parseEscaped(string $line, int $startPos, $interpolated = false, $multiline = false): string
    {
        if ($line[$startPos] != '\'' && $interpolated == false) {
            throw new LineNotManagedException('Not a non interpolated escaped value line');
        }

        if ($line[$startPos] != '"' && $interpolated == true) {
            throw new LineNotManagedException('Not an interpolated escaped value line');
        }

        if ($multiline) {
            $startPos += 3;
        }

        if ($interpolated) {
            $endChar = '"';
        } else {
            $endChar = "'";
        }

        $startPos++;
        $value = '';
        for ($i = $startPos; $i < strlen($line) && $line[$i] != $endChar; $i++) {
            if ($line[$i] == '\\') {
                $escaped = $this->escapeChar($line[$i + 1]);
                if ($escaped !== null) {
                    $value .= $line[$i];
                    $i++;
                    continue;
                }
            }

            if ($line[$i] == '$' && $line[$i + 1] == '{') {
                if ($value != null) {
                    $value .= $this->escapeVar($line, $i);
                    for (; $line[$i] != '}'; $i++);
                    continue;
                }
            }

            $value .= $line[$i];
        }

        if ($line[$i] != $endChar) {
            throw new MalformedException('Unexpected end of line (reading escaped value)');
        }

        if ($multiline) {
            $i += 3;
        } else {
            $i += 1;
        }
        if (trim(substr($line, $i)) != '' && substr(trim(substr($line, $i)), 0, 1) != '#') {
            var_dump($line);
            var_dump($i);
            var_dump(trim(substr($line, $i)));
            throw new MalformedException('Unexpected chars after escaped value');
        }

        return $value;
    }

    /**
     * Parse escaped char
     * @param string $char
     * @return string|null
     */
    private function escapeChar(string $char): string|null
    {
        $escaped = [
            'n' => "\n",
            'r' => "\r",
            't' => "\t",
            'f' => "\f",
            '"' => '"',
            '\'' => '\'',
            '\\' => "\\",
        ];

        if (!array_key_exists($char, $escaped)) {
            return null;
        }

        return $escaped[$char];
    }

    /**
     * Parse escaped var
     * @param string $line
     * @param int $pos
     * @return string|null
     */
    private function escapeVar(string $line, int $pos): string|null
    {
        $match = [];
        if (preg_match('/^\${([a-zA-Z_]+[a-zA-Z0-9_]*)}/', substr($line, $pos), $match) == 1) {
            $var = $match[1];

            if (!array_key_exists($var, $this->vars)) {
                throw new MissingException('The var ' . $var . ' is not known at this point');
            }

            return $this->vars[$var];
        }

        return null;
    }

}